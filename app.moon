lapis = require "lapis"
util = require "lapis.util"
import capture_errors_json, json_params, respond_to from require "lapis.application"
import Model from require "lapis.db.model"

class Products extends Model
class Categories extends Model


class extends lapis.Application
  handle_error: (err, trace) => { json: {error: err} }
  [products: "/products"]: respond_to {

    GET: => { json: Products\select! }
    
    POST: json_params =>
      product = Products\create {
	name: @params.name
	category: @params.category
	price: @params.price
      }
      {status: 201, json: {id: product.id}}

  }
  [categories: "/categories"]: respond_to {

    GET: => { json: Categories\select! }
    
    POST: capture_errors_json json_params =>
      category = Categories\create{
  	name: @params.name
      }
      {status: 201, json: {id: category.id}}

  }
  [category: "/categories/:id"]: respond_to {
    before: =>
      if (Categories\count "id = ?", @params.id) == 0
      	@write status: 404, json: {error: "Not Found"}

    GET: => { json: Categories\find tonumber(@params.id) }

    PUT: capture_errors_json json_params => 
      category = Categories\find @params.id
      category\update {
	name: @params.name
      }
      { status: 204 }
    	

    DELETE: => 
      category = Categories\find tonumber(@params.id)
      category\delete!
      { status: 204 }
  }

  [category: "/products/:id"]: respond_to {
    before: =>
      if (Products\count "id = ?", @params.id) == 0
      	@write status: 404, json: {error: "Not Found"}

    GET: => { json: Products\find @params.id }

    PUT: json_params =>
      prod = Products\find tonumber(@params.id)
      prod\update {
        name: @params.name
        price: tonumber(@params.price)
        category: tonumber(@params.category)
      }
      { status: 204 }

    DELETE: => 
      product = Products\find @params.id
      product\delete!
      { status: 204 }
  }
