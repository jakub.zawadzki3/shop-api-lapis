# Simple shop API with Lapis and Moonscript

CRUD API for products and categories.

## Requirements
- Lapis
Use prebuild docker image e.g.
```bash
docker run --network host -p 8080:8080 -it -v <path_to_code>:<path_in_container> mileschou/lapis /bin/bash
```
- Postgres
Also with prebuild image
```bash
sudo docker run --network host --name some-postgres -e POSTGRES_PASSWORD=password -d -p 5432:5432 postgres:12.10
```
- Define tables
Create following tables in the running instance
```SQL
CREATE TABLE products
            (
                id       SERIAL PRIMARY KEY,
                name     text,
                category INTEGER,
                price    NUMERIC
            );
```
```SQL
CREATE TABLE categories 
            (
                id       SERIAL PRIMARY KEY,
                name     text
            );
```
Optionally add constraint:
```SQL
ALTER TABLE products ADD CONSTRAINT catfk FOREIGN KEY (category) REFERENCES categories (id);
```
- Start Api server in container with Lapis
```bash
moonc *.moon
```
```bash
lapis server
```

## Example request to the API:
Add category
```bash
curl -v -X POST http://localhost:8080/categories -H 'Content-Type: application/json' -d '{"name": "food"}'
```
Add product
```bash
curl -v -X POST http://localhost:8080/products -H 'Content-Type: application/json' -d '{"name": "fish", "category": 1, "price": 80}'
```
Update product
```bash
curl -v -X PUT http://localhost:8080/products/3 -H 'Content-Type: application/json' -d '{"name": "carrot", "category": 1, "price": 10}'
```
Get product
```bash
curl http://localhost:8080/products/1
```
Delete product
```bash
curl -X DELETE http://localhost:8080/products/2
```

