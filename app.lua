local lapis = require("lapis")
local util = require("lapis.util")
local capture_errors_json, json_params, respond_to
do
  local _obj_0 = require("lapis.application")
  capture_errors_json, json_params, respond_to = _obj_0.capture_errors_json, _obj_0.json_params, _obj_0.respond_to
end
local Model
Model = require("lapis.db.model").Model
local Products
do
  local _class_0
  local _parent_0 = Model
  local _base_0 = { }
  _base_0.__index = _base_0
  setmetatable(_base_0, _parent_0.__base)
  _class_0 = setmetatable({
    __init = function(self, ...)
      return _class_0.__parent.__init(self, ...)
    end,
    __base = _base_0,
    __name = "Products",
    __parent = _parent_0
  }, {
    __index = function(cls, name)
      local val = rawget(_base_0, name)
      if val == nil then
        local parent = rawget(cls, "__parent")
        if parent then
          return parent[name]
        end
      else
        return val
      end
    end,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  if _parent_0.__inherited then
    _parent_0.__inherited(_parent_0, _class_0)
  end
  Products = _class_0
end
local Categories
do
  local _class_0
  local _parent_0 = Model
  local _base_0 = { }
  _base_0.__index = _base_0
  setmetatable(_base_0, _parent_0.__base)
  _class_0 = setmetatable({
    __init = function(self, ...)
      return _class_0.__parent.__init(self, ...)
    end,
    __base = _base_0,
    __name = "Categories",
    __parent = _parent_0
  }, {
    __index = function(cls, name)
      local val = rawget(_base_0, name)
      if val == nil then
        local parent = rawget(cls, "__parent")
        if parent then
          return parent[name]
        end
      else
        return val
      end
    end,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  if _parent_0.__inherited then
    _parent_0.__inherited(_parent_0, _class_0)
  end
  Categories = _class_0
end
do
  local _class_0
  local _parent_0 = lapis.Application
  local _base_0 = {
    handle_error = function(self, err, trace)
      return {
        json = {
          error = err
        }
      }
    end,
    [{
      products = "/products"
    }] = respond_to({
      GET = function(self)
        return {
          json = Products:select()
        }
      end,
      POST = json_params(function(self)
        local product = Products:create({
          name = self.params.name,
          category = self.params.category,
          price = self.params.price
        })
        return {
          status = 201,
          json = {
            id = product.id
          }
        }
      end)
    }),
    [{
      categories = "/categories"
    }] = respond_to({
      GET = function(self)
        return {
          json = Categories:select()
        }
      end,
      POST = capture_errors_json(json_params(function(self)
        local category = Categories:create({
          name = self.params.name
        })
        return {
          status = 201,
          json = {
            id = category.id
          }
        }
      end))
    }),
    [{
      category = "/categories/:id"
    }] = respond_to({
      before = function(self)
        if (Categories:count("id = ?", self.params.id)) == 0 then
          return self:write({
            status = 404,
            json = {
              error = "Not Found"
            }
          })
        end
      end,
      GET = function(self)
        return {
          json = Categories:find(tonumber(self.params.id))
        }
      end,
      PUT = capture_errors_json(json_params(function(self)
        local category = Categories:find(self.params.id)
        category:update({
          name = self.params.name
        })
        return {
          status = 204
        }
      end)),
      DELETE = function(self)
        local category = Categories:find(tonumber(self.params.id))
        category:delete()
        return {
          status = 204
        }
      end
    }),
    [{
      category = "/products/:id"
    }] = respond_to({
      before = function(self)
        if (Products:count("id = ?", self.params.id)) == 0 then
          return self:write({
            status = 404,
            json = {
              error = "Not Found"
            }
          })
        end
      end,
      GET = function(self)
        return {
          json = Products:find(self.params.id)
        }
      end,
      PUT = json_params(function(self)
        local prod = Products:find(tonumber(self.params.id))
        prod:update({
          name = self.params.name,
          price = tonumber(self.params.price),
          category = tonumber(self.params.category)
        })
        return {
          status = 204
        }
      end),
      DELETE = function(self)
        local product = Products:find(self.params.id)
        product:delete()
        return {
          status = 204
        }
      end
    })
  }
  _base_0.__index = _base_0
  setmetatable(_base_0, _parent_0.__base)
  _class_0 = setmetatable({
    __init = function(self, ...)
      return _class_0.__parent.__init(self, ...)
    end,
    __base = _base_0,
    __name = nil,
    __parent = _parent_0
  }, {
    __index = function(cls, name)
      local val = rawget(_base_0, name)
      if val == nil then
        local parent = rawget(cls, "__parent")
        if parent then
          return parent[name]
        end
      else
        return val
      end
    end,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  if _parent_0.__inherited then
    _parent_0.__inherited(_parent_0, _class_0)
  end
  return _class_0
end
